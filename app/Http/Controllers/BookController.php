<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Book;

class BookController extends Controller
{
  public function index()
  {
    $books = DB::table('book')->get();
    return view('book.index', ['listbooks' => $books]);
  }

  public function eliminar($id)
  {
    DB::delete('delete from book where id = ?',[$id]);
    return redirect('/books');
  }

  public function agregar()
  {
    return view('book.formagregar');
  }
  public function addbook(Request $request)
  {
    $book = new Book;
    $book->title = $request["txtTitle"];
    $book->author = $request["txtAutor"];
    $book->pages = $request["txtPages"];
    $book->price = $request["txtPrice"];
    $book->description = $request["txtDesc"];
    $book->save();
    return redirect('/books');
  }

  public function modificar($id)
  {
    $book = Book::where('id', $id)
                         ->first();
    return view('book.formmodificar', ['book'=>$book]);
  }
  public function updatebook(Request $request)
  {
    $UpdateBook = Book::where('id', $request["txtId"])->update(array('title' => $request["txtTitle"], 'author' => $request["txtAutor"], 'pages' => $request["txtPages"], 'price' => $request["txtPrice"], 'description' => $request["txtDesc"]));
    return redirect('/books');
  }

  

}
