<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;


class LoginController extends Controller
{
  public function index()
  {
    return view('login.index');
  }

  public function consultalogin(Request $request)
  {
    $user = User::select('id', 'name', 'email', 'password')
                    ->where('email', $request["txtUsuario"])
                    ->first();
    if ($user === null) {
      return redirect('/');
    }else{
      if($user->email===$request["txtUsuario"] && $user->password===$request["txtPass"] ){
        return redirect('/books');
      }else{
        return redirect('/');
      }
    }
    
  }

}
