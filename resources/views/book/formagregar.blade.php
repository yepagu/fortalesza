@extends('layouts.app')
@section('content')
<div class="container">
	
	<form action="{{ url('books/add') }}" method="post">
	<div class="row">
		@csrf
		<div class="col-lg-6">
			<label for="txtTitle">Titulo</label>
			<input type="text" class="form-control" name="txtTitle" placeholder="Titulo de la obra" required>
		</div>
		<div class="col-lg-6">
			<label for="txtAutor">Autor</label>
			<input type="text" class="form-control" name="txtAutor" placeholder="Autor de la Obra" required>
		</div>
		<div class="col-lg-6">
			<label for="txtTitle">Cantidad Paginas</label>
			<input type="number" class="form-control" name="txtPages" placeholder="No. Paginas, Eje: 100" required>
		</div>
		<div class="col-lg-6">
			<label for="txtTitle">$ Precio</label>
			<input type="number" class="form-control" name="txtPrice" placeholder="Eje: 100000" required>
		</div>
		<div class="col-lg-12">
		    <label for="txtDesc">Descripcion</label>
		    <textarea class="form-control" name="txtDesc" rows="3"></textarea>
		 </div>
		<div class="col-lg-12 text-center">
			<br>
			<button type="submit" class="btn btn-success">Guardar</button>
		</div>
	</div>
	</form>
</div>
@endsection