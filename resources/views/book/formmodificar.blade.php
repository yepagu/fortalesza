@extends('layouts.app')
@section('content')
<div class="container">
	
	<form action="{{ url('books/actualizar') }}" method="post">
	<div class="row">
		@csrf
		<input type="hidden" name="txtId" value="{{ $book->id }}">
		<div class="col-lg-6">
			<label for="txtTitle">Titulo</label>
			<input type="text" class="form-control" name="txtTitle" placeholder="Titulo de la obra" value="{{ $book->title }}" required>
		</div>
		<div class="col-lg-6">
			<label for="txtAutor">Autor</label>
			<input type="text" class="form-control" name="txtAutor" placeholder="Autor de la Obra" value="{{ $book->author }}" required>
		</div>
		<div class="col-lg-6">
			<label for="txtTitle">Cantidad Paginas</label>
			<input type="number" class="form-control" name="txtPages" placeholder="No. Paginas, Eje: 100" value="{{ $book->pages }}" required>
		</div>
		<div class="col-lg-6">
			<label for="txtTitle">$ Precio</label>
			<input type="number" class="form-control" name="txtPrice" placeholder="Eje: 100000" value="{{ $book->price }}" required>
		</div>
		<div class="col-lg-12">
		    <label for="txtDesc">Descripcion</label>
		    <textarea class="form-control" name="txtDesc" rows="3">{{ $book->description }}</textarea>
		 </div>
		<div class="col-lg-12 text-center">
			<br>
			<button type="submit" class="btn btn-success">Guardar</button>
		</div>
	</div>
	</form>
</div>
@endsection