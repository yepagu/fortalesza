@extends('layouts.app')
@section('content')
<div id="container">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-3">
            <a href="{{ url('/books/agregar')}}">Agregar</a>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" class="text-center">#</th>
                        <th scope="col" class="text-center">TITULO</th>
                        <th scope="col" class="text-center">AUTOR</th>
                        <th scope="col" class="text-center">PRICE</th>
                        <th scope="col" class="text-center">MODIFICAR</th>
                        <th scope="col" class="text-center">ELIMINAR</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($listbooks as $book)
                    <tr>
                        <td>{{ $book->id }}</td>
                        <td>{{ $book->title }}</td>
                        <td>{{ $book->author }}</td>
                        <td>{{ $book->price }}</td>
                        <td><a href="{{ url('/books/modificar/'.$book->id) }}">Modificar</a></td>
                        <td><a href="{{ url('/books/eliminar/'.$book->id) }}" onclick="return confirm('Esta seguro de eliminar este registro de Libro?')">Eliminar</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection