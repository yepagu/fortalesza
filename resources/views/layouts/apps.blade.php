<!DOCTYPE html>

<html lang="es">

   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

      

      <title>@if(isset($titt)) {{$titt}}  | @endif {{$metas->title}}</title>

      <meta name="description" content="@if(isset($tit)) {{$tit}}  | @endif {{$metas->description}}">

      <meta name="author" content="{{$metas->author}}">

      <meta name="Subject" content="{{$metas->subject}}">

      <meta name="keywords" content="{{$metas->keywords}}">

      <meta name="robots" content="index,follow">

      <meta NAME="Language" CONTENT="Spanish">

      <meta NAME="Revisit" CONTENT="1 day">

      <meta NAME="Distribution" CONTENT="Global">

      <meta http-equiv="cache-control" content="max-age=0" />

      <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />

      <meta http-equiv="pragma" content="no-cache" />

      <meta http-equiv="Expires" content="0">

      <meta http-equiv="Last-Modified" content="0">

      

      <link rel="shortcut icon" href="{{ url('public/img/favicon.ico') }}" />

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <link rel="stylesheet" href="{{ url('public/css/owl.carousel.css') }}">

      <link rel="stylesheet" href="{{ url('public/css/owl.carousel-2.3.4.css') }}">

      <link rel="stylesheet" href="{{ url('public/css/camera.css') }}">

      <!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->

      <link rel="stylesheet" type="text/css" href="{{ url('public/menuTopper/engine7/style.css') }}" />

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.css">

      <link rel="stylesheet" type="text/css" href="{{ url('public/css/bootstrap-material-design.css') }}">

      <link rel="stylesheet" type="text/css" href="{{ url('public/css/bootstrap.css') }}">

      <link rel="stylesheet" type="text/css" href="{{ url('public/css/ripples.css') }}">

      <link rel="stylesheet" href="{{ url('public/css/polisuperior.css') }}">

      <link href="{{ url('public/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

      
    
      
   </head>
  

   <body>

<div id="whatfooter">
    
   <div id="sociallateral">
         <a href="https://api.whatsapp.com/send?phone=573184723336" target="_blank">
             <img src="public/img/whatsapp.png" alt="Whatsapp Occidente" title="Whatsapp Occidente" />
         </a>
   </div>
    
   <div id="sociallateral">
         <a href="https://api.whatsapp.com/send?phone=573023719915" target="_blank">
             <img src="public/img/whatsapp.png" alt="Whatsapp Cafetero" title="Whatsapp Cafetero" />
         </a>
   </div>
   
</div>
<a class="btn btn-primary letragrande" id="btnfixedleft" href="https://www.polisuperior.edu.co/pqrs" target="_blank">PQRS</a>


      <div class="topbar">

         <div class="left">

            <!--<div class="ig ingreso"><a>Ingresar</a></div>

            <div class="ig email"><a href="https://login.live.com/" target="_blank">Email</a></div>-->

         </div>

         <div class="right">

            <div class="redes">

               <a href="https://www.facebook.com/poli.superior.occidente/" class="fb" target="_blank"></a>

               <a href="https://twitter.com/politecnicoOcci" class="tw" target="_blank"></a>

               <a href="https://www.instagram.com/institucionpolisuperior/" class="in" target="_blank"></a>

               <a href="https://www.youtube.com/channel/UCzpDI8fts1cldqQTx-nKBQg" class="yt" target="_blank"></a>

            </div>

            <!--<div class="ig pqr"><a href="{{ url('pqr') }}">PQR</a></div>-->
            <div class="ig pqr"><a href="https://www.polisuperior.edu.co/pqrs" target="_blank">PQRS</a></div>

            <div class="ig contacto"><a href="{{ url('contactenos') }}">Contáctenos</a></div>

         </div>

      </div>

      <header>

         <a href="{{ url('/') }}" class="logo">

            <link rel="stylesheet" type="text/css" href="{{ url('public/logo/engine11/style.css') }}" />

            <script type="text/javascript" src="{{ url('public/logo/engine11/jquery.js') }}"></script>

            <div id="wowslider-container11" style="padding-left: 30px;" class="moverlogo">

               <div class="ws_images">

                  <ul>
                    
                     <li><img src="{{ url('public/logo/data11/images/logopolitecnicooccidente.jpg') }}" alt="Polisuperior cali" id="wows11_0"/></li>
                     <li><img src="{{ url('public/logo/data11/images/logopolitecnicocafetero.jpg') }}" alt="Polisuperior cafetero" id="wows11_1"/></li>
                     <li><img src="{{ url('public/logo/data11/images/logo-politecnico-latinoamericano.jpg') }}" alt="Polisuperior Latinoamericano" id="wows11_2"/></li>

                  </ul>

               </div>

               <div class="ws_shadow"></div>

            </div>

            <script type="text/javascript" src="{{ url('public/logo/engine11/wowslider.js') }}" async></script>

            <script type="text/javascript" src="{{ url('public/logo/engine11/script.js') }}" async></script>

         </a>
        <!-- <a href="https://www.psepagos.co/PSEHostingUI/ShowticketOffice.aspx?ID=8771" alt="pago online polisuperior cafetero" target="_blank" class="payonlinee">
             <div class="payonline">
                 <img src="public/img/pago-online.png" alt="pago online polisuperior cafetero" />Pago Online
             </div>
         </a>-->

         <nav>

            <!-- <a href="https://www.psepagos.co/PSEHostingUI/ShowTicketOffice.aspx?ID=6409" target="_blank"><img src="{{ url('public/img/header/boton-pse.png') }}" alt="pago en linea Polisuperior" style="width: 16%"></a> -->

            <ul class="menu2">

               <li>

                  <a>Estudiantes</a>

                  <ul class="submenu2">

                     <!-- <li><a href="webgrafias.php">Webgrafías</a></li> -->

                     <li><a href="{{ url('corresponsales') }}">Corresponsales Bancarios</a></li>

                     <li >

                        <a>Notas</a>

                        <ul class="subminu" id="programas" style="width: 130px;">

                           <li><a href="http://polisuperiorbarranquilla.q10.com" target="_blank">Barranquilla</a></li>

                           <li><a href="http://politecnicodeoccidente.q10academico.com" target="_blank">Cali</a></li>

                           <li><a href="http://superiorcafetero.q10academico.com" target="_blank">Pereira</a></li>
                           
                           <li><a href="https://polisuperiorpalmira.q10.com/" target="_blank">Palmira</a></li>

                        </ul>

                     </li>

                     <li><a href="{{ url('reconocimientos') }}">Reconocimientos</a></li>

                     <!-- <li><a href=horarios.php">Horarios 2016 B</a></li> -->

                     <!--<li><a href=excelencia-academica.php">Excelencia Académica</a></li>

                        <li><a href=liderazgo-participacion.php">Liderazgo y participación</a></li>

                        <li><a href=trabajo-certificacion.php">Trabajo de Certificación</a></li>-->

                     <!-- <li><a href="{{ url('diplomados') }}">Diplomados y seminarios</a></li> -->

                     <!--<li><a href=ganadores-deportes.php">Ganadores deportes</a></li>

                        <li><a>Cronograma académico</a></li>-->

                     <!-- <li><a href="{{ url('convenios') }}">Convenios</a></li> -->

                     <!--<li><a href=representacion-estudiantil.php">Representación Estudiantil</a></li>-->

                     <!-- <li><a href="{{ url('tramites') }}">Trámites Académicos</a></li> -->

                     <!--<li><a href=reserva-espacios-equipos.php">Reserva De Espacios Y Equipos</a></li>-->

                  </ul>

               </li>

               <li>

                  <a>Docentes</a>

                  <ul class="submenu2">

                     <!-- <li><a href="webgrafias.php">Webgrafías</a></li> -->

                     <li>

                        <a>Notas</a>

                        <ul class="subminu" id="programas" style="width: 130px;">

                           <li><a href="http://polisuperiorbarranquilla.q10.com" target="_blank">Barranquilla</a></li>
                           
                           <li><a href="http://politecnicodeoccidente.q10academico.com" target="_blank">Cali</a></li>

                           <li><a href="http://superiorcafetero.q10academico.com" target="_blank">Pereira</a></li>
                           
                           <li><a href="https://polisuperiorpalmira.q10.com/" target="_blank">Palmira</a></li>

                        </ul>

                     </li>

                     <!--<li><a href=capacitaciones.php">Capacitaciones</a></li>

                        <li><a>Cronogramas</a></li>-->

                     <!-- <li><a href="{{ url('guias') }}">Guías</a></li> -->

                     <!-- <li><a href=reserva-espacios-equipos.php">Reserva de Espacios y Equipos</a></li>-->

                  </ul>

               </li>

               <!-- <li><a>Egresados</a>

                  <ul class="submenu2 subleft">

                    <li><a href=actualizacion-datos-egresado.php">Actualización de Datos</a></li>

                    <li><a href=diplomados.php">Diplomado y Seminarios</a></li>

                    <li><a href=bolsa-empleo.php">Bolsa de Empleo</a></li>

                    <li><a href=convenios.php">Convenios</a></li>

                    <li><a href=representacion-egresados.php">Representación</a></li>

                    <li><a href=tramites.php">Tramites</a></li>

                    <li><a href=lo-que-hacen-egresados.php">Lo que hacen nuestros Egresados?</a></li>

                  </ul>

                  </li> -->

               <!--  <li>

                  <a href="{{ url('convenios_empresa') }}">Convenios</a>

               </li> -->

               <li>

                  <a>Empresas</a>

                  <ul class="submenu2 subleft">

                     <!--<li><a data-type='zoomout' class="button btnTransNegro btnBoxShadow">Mi cuenta</a></li>-->

                     <li><a href="{{ url('admin') }}"  class="button btnTransNegro btnBoxShadow">Mi cuenta</a></li>

                     <!-- <li>

                        <a>Capacitaciones</a>

                        <ul class="subminu" id="programas" style="width: 130px;">

                           <li><a>Cursos</a></li>

                           <li><a href="{{ url('diplomados_empresa') }}">Diplomados</a></li>

                        </ul>

                     </li> -->

                     <li>

                        <a>Solicitud de Pasante</a>

                        <ul class="subminu" id="programas" style="width: 130px;">

                           <li><a href="{{ url('pasante_regular') }}">Pasante Regular</a></li>

                           <li><a href="{{ url('pasante_sena') }}">Pasante Sena</a></li>

                        </ul>

                     </li>

                     <li><a href="{{ url('practica') }}">Prácticas</a></li>

                     <li><a href="{{ url('oferta') }}">Oferta Laboral Egresado</a></li>

                  </ul>

               </li>

            </ul>

            <div id="wowslider-container7" class="ocultarrespon">

               <div class="ws_images">

                  <!-- <p>

                     <a href="http://oferta.senasofiaplus.edu.co/sofia-oferta/"  target="_blank" >

                     <img src="{{ url('public/menuTopper/data7/images/sena.jpg') }}" alt="sena" title="sena" id="wows7_1"/></a>

                     </p> -->

                  <ul>

                     <!--<li data-link="bureau"><a href="http://www.bureauveritas.com.co/" target="_blank">

                        <img src="{{ url('public/menuTopper/data7/images/logo-bureau-veritas-mini.jpg') }}" alt="bureau veritas" title="bureau veritas" id="wows7_0"/></a>

                        </li>-->

                     <li data-link="sena"><a href="http://oferta.senasofiaplus.edu.co/sofia-oferta/"  target="_blank" >

                        <img src="{{ url('public/menuTopper/data7/images/sena.jpg') }}" alt="sena" title="sena" id="wows7_1"/></a>

                     </li>

                     <li data-link="sena"><a href="http://oferta.senasofiaplus.edu.co/sofia-oferta/"  target="_blank" >

                        <img src="{{ url('public/menuTopper/data7/images/sena.jpg') }}" alt="sena" title="sena" id="wows7_1"/></a>

                     </li>

                  </ul>

               </div>

            </div>

         </nav>

      </header>

      <nav>

         <div class="wrap">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#centro" aria-controls="centro" aria-expanded="false" aria-label="Toggle navigation"> 

              <span class="sr-only">Menú</span> 

              <span class="icon-bar"></span> 

              <span class="icon-bar"></span> 

              <span class="icon-bar"></span>

            </button>





            <ul class="menu" id="centro">

               <li class="active"><a href="{{ url('/') }}">Inicio</a></li>

               <li id="institucion">

                  <a>Institución</a>

                  <ul class="submenu">

                     <li><a href="{{ url('quienes-somos') }}">Quienes Somos, misión, visión</a></li>

                     <li><a href="{{ url('diferencia') }}">Qué nos diferencia</a></li>

                     <!-- <li><a href="{{ url('estructura') }}">Estructura Organizacional</a></li>

                        <li><a href="{{ url('manual') }}">Manual de Convivencia</a></li>

                        <li><a href="{{ url('bienestar') }}">Bienestar Estudiantil</a></li>

                        <li><a href="{{ url('certificaciones') }}">Certificaciones</a></li>

                        <li><a href="{{ url('documentos') }}">Documentos institucionales</a></li> -->

                     <!-- <li><a href=nuestra-familia.php">Nuestra Familia</a></li> -->

                     <li><a href="{{ url('public/pdf/POLITICA-DE-PROTECCION-DE-DATOS.pdf') }}" target="_blank">Política y Protección de Datos</a></li>

                     <li><a href="{{ url('public/pdf/CO-MA-01 MANUAL DE CONVIVENCIA POLISUPERIOR  V9.pdf') }}" target="_blank">Manual de Convivencia</a></li>

                     <li><a href="https://polisuperior.netsaia.com/polisuperior/saia/index.php" target="_blank">SAIA</a></li>

                     <!-- <li><a>Resoluciones</a>

                        <ul class="subminu" id="resolucion">

                        <li><a href="{{ url('resoluciones_cali') }}">Cali</a></li>

                        <li><a href="{{ url('resoluciones_pereira') }}">Pereira</a></li>

                        </ul>

                        </li> -->

                  </ul>

               </li>

               <li id="occidente">

                  <a>Poli Occidente</a>

                  <ul class="submenu">

                     <li>

                        <a>Programas</a>

                        <ul class="subminu" id="programas">

                           <li><a href="{{ url('auxiliar_grafico') }}">Auxiliar en Diseño Gráfico</a></li>

                           <!-- <li><a href="{{ url('auxiliar_moda') }}">Auxiliar en Diseño de Modas</a></li> -->

                           <li><a href="{{ url('mercadeo_publicidad') }}">Asistente de Mercadeo y Publicidad</a></li>

                           <li><a href="{{ url('sistemas_informaticos') }}">Auxiliar en sistemas informáticos</a></li>

                           <li><a href="{{ url('contabilidad_finanzas') }}">Asistente en Contabilidad y Finanzas</a></li>

                           <li><a href="{{ url('comercio_exterior') }}">Asistente en Comercio Exterior</a></li>

                           <li><a href="{{ url('seguridad_ocupacional') }}">Seguridad Ocupacional</a></li>

                           <li><a href="{{ url('preparacion_fisica') }}">Preparación Física</a></li>

                        </ul>

                     </li>

                     <li><a href="{{ url('inscripciones') }}">Inscripciones</a></li>

                     <!-- <li><a href="{{ url('resolucion_occidente') }}">Resoluciones</a></li> -->

                     <li><a href="{{ url('actividades_occidente') }}">Actividades y Eventos</a></li>

                     <li><a href="{{ url('https://www.psepagos.co/PSEHostingUI/ShowTicketOffice.aspx?ID=6395') }}" target="_blank">Pago en linea</a></li>

                     <!-- <li><a href=horarios.php">Horarios</a></li> -->

                     <li><a href="oferta-diplomados-polioccidente">Diplomados</a></li>

                  </ul>

               </li>

               <li id="occidente">

                  <a>Poli Cafetero</a>

                  <ul class="submenu">

                     <li>

                        <a>Programas</a>

                        <ul class="subminu" id="programas">

                           <li><a href="{{ url('diseno_grafico') }}">Diseño Gráfico</a></li>

                           <li><a href="{{ url('auxiliar_mercadeo') }}">Publicidad Y Marketing Digital</a></li>

                           <li><a href="{{ url('asistente_contabilidad') }}">Contabilidad y Finanzas</a></li>

                           <li><a href="{{ url('auxiliar_sistemas') }}">Sistemas informáticos</a></li>

                           <li><a href="{{ url('cocina_internacional') }}">Cocina Nacional e Internacional</a></li>

                           <li><a href="{{ url('comercio_exterior_cafetero') }}">Comercio Exterior</a></li>

                           <li><a href="{{ url('seguridad_ocupacional_cafetero') }}">Seguridad y Salud en el Trabajo </a></li>

                           <li><a href="{{ url('preparacion_fisica') }}">Preparación Física </a></li>

                        </ul>

                     </li>

                     <li><a href="{{ url('inscripciones') }}">Inscripciones</a></li>

                     <!-- <li><a href="{{ url('resolucion_cafetero') }}">Resoluciones</a></li> -->

                     <li><a href="{{ url('actividades_cafetero') }}">Actividades y Eventos</a></li>

                     <li><a href="{{ url('https://www.psepagos.co/PSEHostingUI/ShowTicketOffice.aspx?ID=6409') }}" target="_blank">Pago en linea</a></li>

                     <!-- <li><a href=horarios-cafetero.php">Horarios</a></li> -->

                     <li><a href="oferta-diplomados-policafetero">Diplomados</a></li>

                  </ul>

               </li>

               <li><a href="{{ url('inscripciones') }}">Inscripciones</a></li>

               <li><a href="{{ url('deinteres') }}">De Interés</a></li>

               <!-- <li><a>Calendario</a></li> -->

               <li style="width: 100px;">

                  <a target="_blank">Notas</a>

                  <ul class="submenu">

                   <li><a href="http://polisuperiorbarranquilla.q10.com" target="_blank">Barranquilla</a></li>
                           
                     <li><a href="http://politecnicodeoccidente.q10academico.com" target="_blank">Cali</a></li>

                     <li><a href="http://superiorcafetero.q10academico.com" target="_blank">Pereira</a></li>
                     
                     <li><a href="https://polisuperiorpalmira.q10.com/" target="_blank">Palmira</a></li>

                  </ul>

               </li>

               <li>

                  <a>Multimedia</a>

                  <ul class="submenu" style="width: 120px">

                     <li>

                        <a>Fotografías</a>

                        <ul class="subminu" id="programas" style="width: 130px;">

                           <li><a href="{{ url('galeria_poli_cafetero') }}">Poli Cafetero</a></li>

                           <li><a href="{{ url('galeria_poli_occidente') }}">Poli Occidente</a></li>

                        </ul>

                     </li>

                     <li>

                        <a>Videos</a>

                        <ul class="subminu" id="programas" style="width: 130px;">

                           <li><a href="{{ url('video_poli_cafetero') }}">Poli Cafetero</a></li>

                           <li><a href="{{ url('video_poli_occidente') }}">Poli Occidente</a></li>

                        </ul>

                     </li>

                  </ul>

               </li>

               <!-- <li>

                  <a target="_blank">Biblioteca Virtual</a>

                  <ul class="submenu">

                     <li><a href="{{ url('webgrafia_cali') }}">Cali</a></li>

                     <li><a href="{{ url('webgrafia_pereira') }}">Pereira</a></li>

                  </ul>

               </li> -->

            </ul>

         </div>

      </nav>

      <link rel="stylesheet" href="{{ url('public/css/modal-login.css') }}">

      <!-- files window  modal #1-->

      <script>

         $(document).ready(function() {

         $('.button').click(function() {

         type = $(this).attr('data-type');

         $('.overlay-container').fadeIn(function() {

         window.setTimeout(function(){

         $('.window-container.'+type).addClass('window-container-visible');

         }, 100);

         });

         });

         $('.close').click(function() {

         $('.overlay-container').fadeOut().end().find('.window-container').removeClass('window-container-visible');

         });

         });

      </script>

      <!--close files window modal #1-->

      <div class="overlay-container">

         <div class="window-container zoomout">

            <center>

               <h2>LOGIN</h2>

               <p>Solo usuarios registrados</p>

               <form action="{{ url('ingreso_empresa') }}"  method="post">

                  <input type="text" name="txtLogUsuario" id="txtLogUsuario" required="required" class="text" placeholder="Usuario">

                  <input type="password" name="txtLogClave" id="txtLogClave" required="required" class="text" placeholder="Contraseña">

                  <input type="button" name="btnIngresar" id="btnIngresar" value="Ingresar" class="button1" onclick="login()"><br>

                  <a href="#openModal">

                     <i class="fa fa-unlock-alt inline padding09-0" aria-hidden="true"></i>

                     <h2 class="inline size08 padding1-0">Recuperar contraseña</h2>

                  </a>

                  <br>

                  <a href="registro-empresa.php">

                     <i class="fa fa-user-plus inline" aria-hidden="true"></i>

                     <h2 class="inline size08">Registrarse</h2>

                  </a>

                  <div id="openModal" class="modalDialogo">

                     <div>

                        <a href="#close" title="cerrar" class="cerrar">

                        <img src="{{ url('public/img/close.png') }}" alt="button close">

                        </a>

                        <center>

                           <h2>RECUPERAR</h2>

                           <p>Solo usuarios registrados</p>

               <form action="login.php" name="" method="post">

               <input type="email" name="txtCorreoOlvido" id="txtCorreoOlvido" class="text" placeholder="Su email" required="required">

               <input type="button" name="" id="" value="Recuperar" class="button1" onclick="restablecer()">

               </form>

               </center>

               </div>

               </div>

               </form>

            </center>

            <span class="close">

            <img src="{{ url('public/img/close.png') }}" alt="button close">

            </span>

         </div>

      </div>

      @yield('content')

      <div id="maps">

         <div class="inspeccion">"Institución de Educación Técnica aprobado por S.E.M. mediante Licencia de Funcionamiento 4143.2.21.9301 del 2009"</div>

         <div class="wrap">

            <div class="sede">

               <div class="info">

                  <p><b>Atención Sede Cali - Colombia</b></p>

                  <p class="direccion">Carrera 38D No 4 -39, Barrio Santa Isabel</p>

                  <p class="telefono">(572) 558 37 77 (PBX)</p>

                  <p class="whatsapp"><a href="https://api.whatsapp.com/send?phone=573184723336&text=Hola, necesito asesor&iacute;a." target="_blank">(+57) 318 472 3336</a></p>

                  <p class="mail"><a href="mailto:servicioalcliente@polisuperior.edu.co">servicioalcliente@polisuperior.edu.co</a> </p>

               </div>

               <a target="_blank" href="https://www.google.com.co/maps/place/Instituto+Polit%C3%A9cnico+Superior+De+Occidente/@3.42639,-76.5483349,17z/data=!4m5!3m4!1s0x8e30a69fd2a91263:0xc6a2ba31b96502a1!8m2!3d3.426647!4d-76.548115">

               <img src="{{ url('public/img/mapas/cali1.jpg') }}" alt="">

               </a>

               <a target="_blank" href="https://www.google.com.co/maps/@3.4265746,-76.5480597,3a,90y,33.6h,79.77t/data=!3m6!1e1!3m4!1swC401_2huE0MbwCFCIeCFg!2e0!7i13312!8i6656">

               <img src="{{ url('public/img/mapas/cali2.jpg') }}" alt="">

               </a>

            </div>

            <div class="sede">

               <div class="info">

                  <p><b>Atención Sede Pereira - Colombia</b></p>

                  <p class="direccion">Calle 4 Bis No 15-31, Sector Circunvalar</p>

                  <p class="telefono">(036) 3170990 - 3161972</p>

                  <p class="whatsapp"><a href="https://api.whatsapp.com/send?phone=573023719915&text=Hola, necesito asesor&iacute;a." target="_blank">(+57) 302 371 9915</a></p>

                  <p class="mail"><a href="mailto:servicioalclientepereira@polisuperior.edu.co">servicioalclientepereira@polisuperior.edu.co</a> </p>

               </div>

               <a target="_blank" href="https://www.google.com.co/maps/place/Cl.+4+Bis,+Pereira,+Risaralda/@4.8071884,-75.682868,18z/data=!4m13!1m7!3m6!1s0x8e38873c70ea760f:0x8dcbe1f8f5daaf8c!2sCl.+4+Bis,+Pereira,+Risaralda!3b1!8m2!3d4.8071724!4d-75.6827151!3m4!1s0x8e38873c70ea760f:0x8dcbe1f8f5daaf8c!8m2!3d4.8071724!4d-75.6827151">

               <img src="{{ url('public/img/mapas/mapa-pereira3.jpg') }}" alt="">

               </a>

               <a target="_blank" href="https://www.google.com/maps/@4.8072353,-75.6827186,3a,90y,140.39h,88.9t/data=!3m6!1e1!3m4!1sA1lrBlZO9wuuyv9AjxnjvA!2e0!7i13312!8i6656?hl=es">

               <img src="{{ url('public/img/mapas/mapa-pereira2.jpg') }}" alt="">

               </a>

            </div>

            <div class="pqr">

               <br><a href="{{ url('contactenos') }}"><img src="{{ url('public/img/mapas/enviar-mensaje.jpg') }}" alt="enviar mensaje" title="enviar mensaje"></a>

               <p>Enviar Mensaje</p>

               <br>

               <!--<a href="{{ url('pqr') }}"><img src="{{ url('public/img/mapas/peticiones-quejas-reclamos.jpg') }}" alt="peticiones, quejas y reclamos" title="peticiones, quejas y reclamos"></a>-->
               <a href="https://www.polisuperior.edu.co/pqrs" target="_blank"><img src="{{ url('public/img/mapas/peticiones-quejas-reclamos.jpg') }}" alt="peticiones, quejas y reclamos" title="peticiones, quejas y reclamos"></a>

               <p>Peticiones, Quejas o Reclamos</p>

               <br>

               <!-- <a href="{{ url('reserva') }}"><img src="{{ url('public/img/mapas/aulas-equipos.jpg') }}" alt="trabaje con nosotros" title="trabaje con nosotros"></a>

               <p>Reserva De Espacios Y Equipos</p> -->

            </div>

         </div>
         
         
         
        <div class="wrap">

            <div class="sede"><br><br><br><br>

               <div class="info">

                  <p><b>Atención Sede Barranquilla - Colombia</b></p>

                  <p class="direccion">Carrera 42H #84B-77, Los Alpes </p>

                  <p class="telefono">(605) 320 8303 - 300 452 2339</p>

                  <p class="whatsapp"><a href="https://api.whatsapp.com/send?phone=573004522339&text=Hola, necesito asesor&iacute;a." target="_blank">(+57) 300 452 2339</a></p>

                  <p class="mail"><a href="mailto:servicioalcliente@polisuperior.edu.co">servicioalcliente@polisuperior.edu.co</a> </p>

               </div>

               <a target="_blank" href="https://www.google.com/maps/place/Cra.+42h+%2384b-77,+Barranquilla,+Atl%C3%A1ntico/@10.9979791,-74.8224844,19z/data=!4m5!3m4!1s0x8ef42d0125fc21d9:0x394d7b5a785aca8c!8m2!3d10.9980331!4d-74.8222618?hl=es">

               <img src="{{ url('public/img/mapas/mapa-barranquilla.jpg') }}" alt="polisuperior barranquilla">

               </a>

               <a target="_blank" href="https://www.google.com/maps/place/Cra.+42h+%2384b-77,+Barranquilla,+Atl%C3%A1ntico/@10.9981351,-74.8222246,3a,90y,207.26h,90.11t/data=!3m6!1e1!3m4!1sjts0B2bIYjOtwhtgs-h4tw!2e0!7i13312!8i6656!4m5!3m4!1s0x8ef42d0125fc21d9:0x394d7b5a785aca8c!8m2!3d10.9980331!4d-74.8222618?hl=es">

               <img src="{{ url('public/img/mapas/foto-barranquilla.jpg') }}" alt="polisuperior barranquilla">

               </a>

            </div>

            <div class="sede"><br><br><br><br>

               <div class="info">

                  <p><b>Atención Sede Palmira - Colombia</b></p>

                  <p class="direccion">Cra 21 #32-56, Uribe Uribe</p>

                  <p class="telefono">(602) 266 1325 - 316 558 4988</p>

                  <p class="whatsapp"><a href="https://api.whatsapp.com/send?phone=573165584988&text=Hola, necesito asesor&iacute;a." target="_blank">(+57) 316 558 4988</a></p>

                  <p class="mail"><a href="mailto:servicioalcliente@polisuperior.edu.co">servicioalclientepereira@polisuperior.edu.co</a> </p>

               </div>

               <a target="_blank" href="https://www.google.com/maps/place/Polit%C3%A9cnico+superior+de+occidente+sede+Palmira/@3.5289255,-76.2930657,19z/data=!4m5!3m4!1s0x8e3a058a5ff00929:0x4b143c485528c6d!8m2!3d3.5287823!4d-76.2926795?hl=es">

               <img src="{{ url('public/img/mapas/mapa-palmira.jpg') }}" alt="polisuperior palmira">

               </a>

               <a target="_blank" href="https://www.google.com/maps/place/Polit%C3%A9cnico+superior+de+occidente+sede+Palmira/@3.5288029,-76.2927145,3a,90y,137.42h,80.42t/data=!3m7!1e1!3m5!1sHI1mCr8y8gi9fskioMmwNg!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3DHI1mCr8y8gi9fskioMmwNg%26cb_client%3Dsearch.gws-prod.gps%26w%3D86%26h%3D86%26yaw%3D127.40018%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656!4m5!3m4!1s0x8e3a058a5ff00929:0x4b143c485528c6d!8m2!3d3.5287823!4d-76.2926795?hl=es">

               <img src="{{ url('public/img/mapas/foto-barranquilla.jpg') }}" alt="polisuperior palmira">

               </a>

            </div>

         </div>
         
         

      </div>

   </body>

   <footer>
    

      <div class="wrap">

         <p>Todos los derechos reservados, POLITECNICO SUPERIOR <br>All copyright 2012 - <?php print date("Y")?></p>

         <div class="creasotol">

            <span>

            <a href="http://www.creasotol.com" target="_blank" alt="Diseño de Paginas Web" title="Diseño de Paginas Web">Diseño Web</a> <br>

            <a href="http://www.creasotol.com" target="_blank" alt="Diseño Web Colombia" title="Diseño Web Colombia">www.creasotol.com</a>

            </span>

         </div>

      </div>

   </footer>

   <script src="{{ url('public/js/bootstrap-modal.js') }}"></script>

   <script src="{{ url('public/js/bootstrap-modalmanager.js') }}"></script>

   <script src="{{ url('public/js/app-min.js') }}"></script>

   <!--script type="text/javascript" src="http://www.polisuperior.edu.co/public/menuTopper/engine7/wowslider.js"></script-->

   <script type="text/javascript" src="{{ url('public/menuTopper/engine7/script.js') }}"></script>

   <script>

      $('#centro li#institucion ul li').width(190);

      $('#centro li#institucion ul.submenu li ul#resolucion').width('auto');

      $('#resolucion li').width(90);

      $('#centro li#occidente ul li').width(140);

      $('#centro li#occidente ul.submenu li ul#programas li').width(230);

      

      if($.browser.mozilla){

      //console.log('mox');

      $('header nav').css('width', 'calc(80%)');

      }

      if($.browser.msie){

      //console.log('msie');

      $('header nav').css('width', 'calc(80%)');

      }

      //width: calc(80%);

      if($.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))){

      $('body').css('overflow', 'hidden');

      $('body').css('width', '1076px');

      }

      function login(){

      var usu = $("#txtLogUsuario").val();

      var cla = $("#txtLogClave").val();

      if (usu != '' && cla != '') {

      $.ajax({

      method: "POST",

      url: "partials/metodos.php",

      data: { accion: "loginAdm", usu: usu, cla: cla }

      })

      .done(function(msg) {

      if (msg == 1) {

      location.href = "/admin2/tienda_productos.php";

      }else{

      alert(msg);

      //location.href = "index.php";

      }

      });

      }else{

      alert("Ingrese su usuario y su clave para ingresar a la tienda.");

      }

      }

      /**

      * [FUNCION logout() REALIZA LA SALIDA DEL USUARIO]

      */

      function logout(){

      $.ajax({

      method: "POST",

      url: "partials/metodos.php",

      data: { accion: "logout" }

      })

      .done(function() {

      location.href = "index.php";

      });

      }

      function restablecer(){

      var correo = $("#txtCorreoOlvido").val()

      if (correo != '') {

      $.ajax({

      method: "POST",

      url: "partials/metodos.php",

      data: { accion: "restablecer", ema: correo }

      })

      .done(function( msg ) {

      if (msg != '') {

      alert("Su clave de acceso fue enviada a su correo.");

      }else{

      alert("El correo ingresado no pertenece a ningun cliente.");

      }

      });

      }else{

      alert("Ingrese su correo para restablecer su contraseÑa.");

      }

      }

   </script>

   <script type="text/javascript" src="{{ url('public/js/liblightbox/scrollingTextMouseover.js') }}"></script>

   <script type="text/javascript" src="{{ url('public/js/liblightbox/jquery.lightbox-0.5.js') }}"></script>

   <script type="text/javascript" src="{{ url('public/js/owl.carousel-2.3.4.min.js') }}"></script>

   <link rel="stylesheet" type="text/css" href="{{ url('public/js/liblightbox/jquery.lightbox-0.5.css') }}" media="screen" />

   <script type="text/javascript" src="{{ url('public/js/bootstrap.min.js') }}"></script>
   

   <script>

      $(document).ready(function() {

       $('#contDetaGaleria a').lightBox();

      });

   </script>

   <script type="text/javascript">

      $('button[data-text]').click(function(e){

      e.preventDefault();

      var element = $(this).attr('data-text');

      $(".botones").hide();

      $("#"+element).show();

      });

   </script>

</html>

