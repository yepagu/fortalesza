@extends('layouts.app')
@section('content')
<div id="contenedor">
    <div id="contenedorcentrado">
        <div id="login">
            <form id="loginform" method="post" action="{{ url('loginin') }}">
                @csrf
                <label for="usuario">Usuario</label>
                <input id="txtUsuario" type="text" name="txtUsuario" placeholder="Usuario" required>
                
                <label for="password">Contraseña</label>
                <input id="txtPass" type="password" placeholder="Contraseña" name="txtPass" required>
                
                <button type="submit" title="Ingresar" name="Ingresar">Login</button>
            </form>
            
        </div>
        <div id="derecho">
            <div class="titulo">
                Bienvenido a Fortalesza
            </div>
            <hr>
            <div class="pie-form">
                <!-- <a href="#">¿Perdiste tu contraseña?</a>
                <a href="#">¿No tienes Cuenta? Registrate</a>
                <hr>
                <a href="#">« Volver</a> -->
            </div>
        </div>
    </div>
</div>
@endsection