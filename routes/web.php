<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\MenulController;
use App\Http\Controllers\BookController;

Route::get('/', [LoginController::class, 'index']);
Route::post('/loginin', [LoginController::class, 'consultalogin']);
Route::get('/books', [BookController::class, 'index']);
Route::get('/books/agregar', [BookController::class, 'agregar']);
Route::post('/books/add', [BookController::class, 'addbook']);
Route::get('/books/modificar/{id}', [BookController::class, 'modificar']);
Route::post('/books/actualizar', [BookController::class, 'updatebook']);
Route::get('/books/eliminar/{id}', [BookController::class, 'eliminar']);
Route::get('/menuprincipal', [MenulController::class, 'menuprin']);